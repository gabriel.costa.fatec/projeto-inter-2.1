using Microsoft.AspNetCore.Mvc;

public class ProdutoController : Controller
{
    IProdutoRepository produtoRepository;

    public ProdutoController(IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

    public ActionResult Index()
    {
        return View(produtoRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Produto model)
    {
        produtoRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int sku)
    {
        Produto p = produtoRepository.Read(sku);
        if(p != null)
            return View(p);

        return NotFound();
    }

    public ActionResult Delete(int sku)
    {
        produtoRepository.Delete(sku);
        return RedirectToAction("Index");
    }

    public ActionResult Update(int sku)
    {
        Produto p = produtoRepository.Read(sku);
        if(p != null)
            return View(p);

        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int sku, Produto model)
    {
        produtoRepository.Update(sku, model);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", produtoRepository.Search(pesquisa));
    }
}