using Microsoft.AspNetCore.Mvc;

public class PedidoController : Controller
{
    IPedidoRepository pedidoRepository;

    public PedidoController(IPedidoRepository pedidoRepository)
    {
        this.pedidoRepository = pedidoRepository;
    }
    
    public ActionResult Index()
    {
        return View(pedidoRepository.Read());
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Pedido model)
    {
        pedidoRepository.Create(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        Pedido p = pedidoRepository.Read(id);
        if(p != null)
            return View(p);

        return NotFound();
    }

    public ActionResult Delete(int id)
    {
        pedidoRepository.Delete(id);
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", pedidoRepository.Search(pesquisa));
    }
}