using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

public class UsuarioController : Controller
{
    IUsuarioRepository usuarioRepository;

    public UsuarioController(IUsuarioRepository usuarioRepository) 
    {
        this.usuarioRepository = usuarioRepository;
    }

    [HttpGet]
    public ActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Login(IFormCollection form)
    {
        string? nomeUsuario = form["nomeUsuario"];
        string? senha = form["senha"];

        var usuario = usuarioRepository.Login(nomeUsuario!, senha!);

        if(usuario == null)
        {
            ViewBag.Error = "Usuário/Senha inválidos";
            return View();
        }

        HttpContext.Session.SetString("nomeUsuario", JsonSerializer.Serialize(usuario));

        return RedirectToAction("Index", "Main");
    }

    [HttpGet]
    public ActionResult Logout()
    {
        HttpContext.Session.Clear();
        return RedirectToAction("Login");
    }
}