using Microsoft.Data.SqlClient;

public class FornecedorRepository : Database, IFornecedorRepository
{
    public void Create(Fornecedor fornecedor)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Fornecedores VALUES (@cnpj, @nome, @email, @telefone)";

        cmd.Parameters.AddWithValue("@cnpj", fornecedor.CNPJ);
        cmd.Parameters.AddWithValue("@nome", fornecedor.Nome);
        cmd.Parameters.AddWithValue("@email", fornecedor.Email);
        cmd.Parameters.AddWithValue("@telefone", fornecedor.Telefone);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Fornecedores WHERE idFornecedor = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }

    public List<Fornecedor> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Fornecedores";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Fornecedor> fornecedores = new List<Fornecedor>();

        while(reader.Read())
        {
            Fornecedor f = new Fornecedor();
            f.IdFornecedor = reader.GetInt32(0);
            f.CNPJ = reader.GetString(1);
            f.Nome = reader.GetString(2);
            f.Email = reader.GetString(3);
            f.Telefone = reader.GetString(4);

            fornecedores.Add(f);
        }

        return fornecedores;

    }

    public Fornecedor Read(int id)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Fornecedores WHERE idFornecedor = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Fornecedor f = new Fornecedor();
            f.IdFornecedor = reader.GetInt32(0);
            f.CNPJ = reader.GetString(1);
            f.Nome = reader.GetString(2);
            f.Email = reader.GetString(3);
            f.Telefone = reader.GetString(4);
            
            return f;
        }

        return null;

    }

    public List<Fornecedor> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Fornecedores WHERE Nome like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Fornecedor> fornecedores = new List<Fornecedor>();

        while(reader.Read())
        {
            Fornecedor f = new Fornecedor();
            f.IdFornecedor = reader.GetInt32(0);
            f.CNPJ = reader.GetString(1);
            f.Nome = reader.GetString(2);
            f.Email = reader.GetString(3);
            f.Telefone = reader.GetString(4);
            
            fornecedores.Add(f);
        }

        return fornecedores;

    }

    void IFornecedorRepository.Update(int id, Fornecedor fornecedor)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Fornecedores SET CNPJ = @cnpj, nome = @nome,  email = @email, telefone = @telefone WHERE idFornecedor = @id";
        cmd.Parameters.AddWithValue("@cnpj", fornecedor.CNPJ);
        cmd.Parameters.AddWithValue("@nome", fornecedor.Nome);
        cmd.Parameters.AddWithValue("@email", fornecedor.Email);
        cmd.Parameters.AddWithValue("@telefone", fornecedor.Telefone);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }
}