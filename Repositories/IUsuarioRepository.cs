public interface IUsuarioRepository
{
    Funcionario? Login(string email, string senha);
}