public interface IPedidoRepository
{
    List<Pedido> Read();
    void Create(Pedido pedido);
    Pedido Read(int id);
    void Delete(int id);
    List<Pedido> Search(string pesquisa);
}