public interface IClienteRepository
{
    List<Cliente> Read();
    void Create(Cliente cliente);
    Cliente Read(int id);
    void Update(int id, Cliente cliente);
    void Delete(int id);
    List<Cliente> Search(string pesquisa);
}