public interface IFuncionarioRepository
{
    List<Funcionario> Read();
    void Create(Funcionario funcionario);
    Funcionario Read(int id);
    void Update(int id, Funcionario funcionario);
    void Delete(int id);
    List<Funcionario> Search(string pesquisa);
}