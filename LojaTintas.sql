create database LojaTintas
go

use LojaTintas
go

create table Clientes
(
	idCliente	int				not null	primary key		identity,
	nome		varchar(50)		not null,
	CPF			varchar(14)		not null	unique,
	telefone	varchar(15)		not null,
	email		varchar(50)			null	unique,
	endereco	varchar(200)	not null
)
go

create table Funcionarios
(
	idFuncionario	int				not null	primary key		identity,
	nomeUsuario		varchar(20)		not null	unique,
	senha			varchar(10)		not null,
	salario			decimal(10,2)	null,
	nome			varchar(50)		not null,
	CPF				varchar(14)		not null	unique,
	endereco		varchar(200)	not null,
	telefone		varchar(15)		not null,
	email			varchar(50)			null	unique
)
go

create table Fornecedores
(
	idFornecedor	int			not null	primary key		identity,
	CNPJ			varchar(18)	not null	unique,
	nome			varchar(50)	not null,
	email			varchar(50)	not null	unique,
	telefone		varchar(15)	not null,
)
go

create table Produtos
(
	SKU				int				not null	primary key		identity,
	fornecedorId	int				not null,
	categoria		varchar(50)		not null,
	descricao		varchar(200)	not null,
	UM				varchar(4)		not null,
	preco			decimal(10,2)	not null,
	qtdEstoque		decimal(10,2)	not null,
	foreign key	(fornecedorId)		references	Fornecedores(idFornecedor)
)
go

create table Pedidos
(
	idPedido		int				not null	primary key		identity,
	data			datetime		not null,
	clienteId		int				not null,
	funcionario		varchar(20)		not null,
	valor			decimal(10,2)	not null,
	foreign key (clienteId)			references	Clientes(idCliente),
	foreign key	(funcionario)		references	Funcionarios(nomeUsuario),

)
go

create table Listas_Produtos
(
	idLista			int				not null	primary key		identity,
	pedidoId		int				not null,
	SKU				int				not null,
	qtd				decimal(10,2)	not null,
	valor			decimal(10,2)	not null,
	foreign key (pedidoId)	references Pedidos(idPedido),
	foreign key (SKU)		references Produtos(SKU)
)
go

create table Movimentacoes
(
	idMov			int				not null	primary key		identity,
	data			datetime		not null,
	status			varchar(10)		not null,
	SKU				int				not null,
	qtdMovimento	int				not null,	
	funcionario		varchar(20)		not null,
	foreign key (SKU)				references Produtos(SKU),
	foreign key	(funcionario)		references	Funcionarios(nomeUsuario)
)
go


select * from Funcionarios

dbcc useroptions

SELECT * FROM Clientes WHERE IdCliente like '%2%'

INSERT INTO Funcionarios VALUES ('admin', 'admin', 10000, 'Gabriel', '44541687844', 'R Osvaldo Cruz, 1566, JD Panorama, Bady Bassitt - SP', '17 997737851', 'admin@admin.com')