public class Funcionario
{
    private int idFuncionario;
    private string nomeUsuario;
    private string? senha;
    private decimal salario;
    private string nome;
    private string cpf;
    private string endereco;
    private string telefone;
    private string email;

    public int IdFuncionario { get => idFuncionario; set => idFuncionario = value; }
    public string NomeUsuario { get => nomeUsuario; set => nomeUsuario = value; }
    public string? Senha { get => senha; set => senha = value; }
    public decimal Salario { get => salario; set => salario = value; }
    public string Nome { get => nome; set => nome = value; }
    public string Cpf { get => cpf; set => cpf = value; }
    public string Endereco { get => endereco; set => endereco = value; }
    public string Telefone { get => telefone; set => telefone = value; }
    public string Email { get => email; set => email = value; }
}