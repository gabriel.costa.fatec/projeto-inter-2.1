public class Pedido
{
    private int idPedido;
    private DateTime data;
    private int clienteId;
    private string funcionario;
    private decimal valor;


    public int IdPedido
    {
        get { return this.idPedido; }
        set { this.idPedido = value; }
    }
    public DateTime Data
    {
        get { return this.data; }
        set { this.data = value; }
    }
    public int ClienteId
    {
        get { return this.clienteId; }
        set { this.clienteId = value; }
    }
    public string Funcionario
    {
        get { return this.funcionario; }
        set { this.funcionario = value; }
    }

    public decimal Valor
    {
        get { return this.valor; }
        set { this.valor = value; }
    }
}